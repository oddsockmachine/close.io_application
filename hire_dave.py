import json
import requests

cover_letter = """Hi there,
I'm a DevOps engineer with a background in Python development.
I've been looking for a role that'd allow me to use both skillsets.
It looks like you've got an interesting bunch of projects and roles with the sort of overlap I'm interested in - it'd be great to find out more about what you're doing and the sort of person you're looking for."""

data = {'first_name': 'David',
        'last_name': 'Walker',
        'email': 'oddsockmachine@gmail.com',
        'phone': '+447530025544',
        'urls': ["https://uk.linkedin.com/in/dave-walker-9b4891ba",
                 "https://bitbucket.org/oddsockmachine/",
                 "https://bitbucket.org/oddsockmachine/close.io_application/src"],
        'cover_letter': cover_letter}

print json.dumps(data)

url = 'https://app.close.io/hackwithus/'
# r = requests.post(url, json=data)
# response_id = r.json().get("id")
response_id = "lead_jgK10NvnJ10jJOgzfRtaxeckC2RnolCYf8eL0q7LCwF"
print response_id

# Sample input list
x = [2, 5, 7, 4, 1, 6, 8, 9, 3, "a", "b", "c", "d"]


# Yield element if (type is int and element is odd) or (index is even)
print list(elem for index, elem in enumerate(x) if (type(elem)==int and elem % 2 != 0) or (index % 2 == 0))
